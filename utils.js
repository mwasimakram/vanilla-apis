const jwt = require("jsonwebtoken");


const TOKEN_SECRET_KEY = "here_is_my_very_secret_key";
module.exports.handleResponse = async (res, status, data) => {
    // set the status code, and content-type
    status = status || 200;
    res.writeHead(status, { "Content-Type": "application/json" });
    // send the data
    res.end(JSON.stringify(data));
}


exports.generateToken = function (user) {
    const payload = {
        id: user._id,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
    };
    return jwt.sign(payload, TOKEN_SECRET_KEY, { expiresIn: "30d", issuer: "https://example.com/" });
};


exports.getReqData = (req) => {
    return new Promise((resolve, reject) => {
        try {
            let body = "";
            // listen to data sent by client
            req.on("data", (chunk) => {
                // append the string version to the body
                body += chunk.toString();
            });
            // listen till the end
            req.on("end", () => {
                // send back the data
                resolve(body);
            });
        } catch (error) {
            reject(error);
        }
    });
}

exports.validateToken = async (req, res, next) => {
    try {
        // eslint-disable-next-line prefer-destructuring
        let bearerToken = req.headers.authorization;

        if (!bearerToken) {
            return handleResponse(res, 401, { error: "Auth Token Not Provided" });
        }

        // added by Sunil
        const parts = bearerToken.split(" ");

        if (parts[0] !== "Bearer") {
            return handleResponse(res, 401, { error: "Invalid Token" });
        }
        bearerToken = parts[1];
        console.warn("[verifyToken][bearerToken]", bearerToken);

        let authData = await jwt.verify(bearerToken, TOKEN_SECRET_KEY);

        console.warn("[validateToken][authData]", authData);
        req.user = authData;
        return { success: true };
    } catch (error) {
        // set the status code and content-type
        res.writeHead(401, { "Content-Type": "application/json" });
        // send the error
        res.end(JSON.stringify({ message: error }));
        return { success: false };

    }

};
