// controller.js
const User = require(`./db/models/user`).model;
const bcrypt = require("bcryptjs");
const { generateToken } = require("./utils");

// getting all Users
exports.getUsers = async () => {
    // return all Users
    return new Promise((resolve, _) => resolve(data));
}

// getting a single User
exports.getUser = async (query) => {
    let projection = {
        first_name: 1,
        last_name: 1,
        email: 1,
    }

    if (query && query.id) {
        query = { _id: query.id };
    } else {
        query = {}
    }
    let user = await User.find(query, projection).lean(true);

    if (!user) {
        return { status: 404, success: false, data: {} };
    }
    return { success: true, data: user };

}

// creating a User
exports.createUser = async (req) => {

    console.log("[registerUser][body]", req.body);
    const {
        first_name,
        last_name,
        email,
        password
    } = req.body;

    const checkUser = await User.findOne({ email }).lean(true);

    if (checkUser) {
        console.error("[register][user][Email address found]");
        return { status: 404, success: false, message: "User with specified email address already exists" }
    };


    const user = new User({
        first_name,
        last_name,
        email
    });

    let salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();

    const token = `Bearer ${await generateToken(user)}`;
    return { success: true, data: token };
}

// updating a User
exports.updateUser = async (id, dataToUpdate) => {
    if (!id) {
        return { status: 422, success: true, data: {} };
    }

    let user = await User.findOne({ _id: id });

    if (!user) {
        return { status: 404, success: false, data: {} };
    }

    if (dataToUpdate.first_name) user.first_name = dataToUpdate.first_name;
    if (dataToUpdate.last_name) user.last_name = dataToUpdate.last_name;
    await user.save();
    return { success: true };
}

// deleting a User
exports.deleteUser = async (id) => {
    if (!id) {
        return { status: 422, success: true, data: {} };
    }

    await User.findByIdAndRemove({ _id: id });

    return { success: true };

}