//app.js
const http = require("http");
const url = require('url');
const { controller: UserController } = require("./controller");
const { createUser, getUser, deleteUser, updateUser } = require("./controller");
const { handleResponse, getReqData, validateToken } = require("./utils");

require("./db/database");

const PORT = process.env.PORT || 80;

const server = http.createServer(async (req, res) => {

    try {

        let reqUrl = url.parse(req.url, true);
        // /api/users : GET
        if (reqUrl.pathname === "/api/users" && req.method === "GET") {
            let validateTokenResult = await validateToken(req, res);
            if (!validateTokenResult.success) {
                return handleResponse(res, 401, { success: false });
            }
            const result = await getUser(reqUrl.query);
            return handleResponse(res, result.status, result.data)
        }

        else if (reqUrl.pathname === "/api/users" && req.method === "DELETE") {
            let validateTokenResult = await validateToken(req, res);
            if (!validateTokenResult.success) {
                return handleResponse(res, 401, { success: false });
            }

            console.error("QUERY", reqUrl.query)
            if (!reqUrl.query) {
                return handleResponse(res, 422, { success: false, message: "Invalid Input Data" });
            }
            const result = await deleteUser(reqUrl.query.id);
            return handleResponse(res, result.status, result.data)
        }

        else if (reqUrl.pathname === "/api/users" && req.method === "PATCH") {
            let validateTokenResult = await validateToken(req, res);
            if (!validateTokenResult.success) {
                return handleResponse(res, 401, { success: false });
            }

            req.body = await getReqData(req);
            console.error("REQ BODY", req.body)
            req.body = JSON.parse(req.body);
            console.error("REQ BODY22", req.body)



            if (!reqUrl.query) {
                return handleResponse(res, 422, { success: false, message: "Invalid Input Data" });
            }
            const result = await updateUser(reqUrl.query.id, req.body);
            return handleResponse(res, result.status, result.data)
        }

        // /api/users/ : POST
        else if (req.url === "/api/users" && req.method === "POST") {

            req.body = await getReqData(req);
            req.body = JSON.parse(req.body);
            const result = await createUser(req);
            return handleResponse(res, result.status, result)
        }

        // No route present
        else {
            res.writeHead(404, { "Content-Type": "application/json" });
            res.end(JSON.stringify({ message: "Route not found" }));
        }
    } catch (error) {
        console.error(error);
        // set the status code and content-type
        res.writeHead(404, { "Content-Type": "application/json" });
        // send the error
        res.end(JSON.stringify({ message: error }));
    }

});

server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});