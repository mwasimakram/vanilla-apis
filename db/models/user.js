const moduleName = "[User]";
const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSchema = new Schema({
    password: { type: String },
    email: { type: String, unique: true, required: true },
    first_name: { type: String },
    last_name: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });

UserSchema.post("save", async function (doc) {
    console.log("%s has been saved", doc._id);
});

exports.model = mongoose.model("User", UserSchema);
console.warn("[UserModel] created successfully");