const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
const moduleName = "[database]";


const options = {
    driver: "mongodb+srv",
    host: "localhost",
    dbName: "my_db",
};

const dbURI = "mongodb://localhost:27017/my_db";

const connectWithRetry = function () {
    return mongoose.connect(dbURI, function (err) {
        if (err)
            console.error("Mongoose failed to connect", err);
    });
};

connectWithRetry();

mongoose.connection.on("connected", function () {
    console.info(".mongoose.connected, connection has been established with " + dbURI);
});

mongoose.connection.on("error", function (err) {
    console.error(".mongoose.error, connection to mongo failed", err);
});

mongoose.connection.on("disconnected", function () {
    console.warn(".mongoose.disconnected connection closed - retrying in 5 sec");
    setTimeout(connectWithRetry, 5000);
});
